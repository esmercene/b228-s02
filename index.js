

// 1. 
	let studentOne = {
    name: "John",
    email: "john@email.com",
    grades: [ 89, 84, 78, 88 ],

    login(){
        console.log(`${this.name} has logged in`);
    },
    logout(){
    console.log(`${this.name} has logged out`);
    },
    listGrades(){
    console.log(`Student one's quarterly averages are ${this.grades}`);
    },

 // 2.   
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
// 3. 
	willPass() {
        return this.computeAve() >= 85
 	},
// 4.
 	willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }


}



let studentTwo = {
    name: "Joe",
    email: "joe@email.com",
    grades: [ 78, 82, 79, 85 ],

    login(){
        console.log(`${this.name} has logged in`);
    },
    logout(){
    console.log(`${this.name} has logged out`);
    },
    listGrades(){
    console.log(`Student two's quarterly averages are ${this.grades}`);
    },
// 2.
   computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
// 3. 
	willPass() {
        return this.computeAve() >= 85
    },
// 4.
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }

}




let studentThree = {
    name: "Jane",
    email: "jane@email.com",
    grades: [ 87, 89, 91, 93 ],

    login(){
        console.log(`${this.name} has logged in`);
    },
    logout(){
    console.log(`${this.name} has logged out`);
    },
    listGrades(){
    console.log(`Student three's quarterly averages are ${this.grades}`);
    },
// 2.
   computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
// 3. 
	willPass() {
        return this.computeAve() >= 85
    },
// 4.
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }

}


let studentFour = {
    name: "Jessie",
    email: "jessie@email.com",
    grades: [ 91, 89, 92, 93 ],

    login(){
        console.log(`${this.name} has logged in`);
    },
    logout(){
    console.log(`${this.name} has logged out`);
    },
    listGrades(){
    console.log(`Student four's quarterly averages are ${this.grades}`);
    },

//2.
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
// 3. 
	willPass() {
        return this.computeAve() >= 85
    },

 // 4.
     willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }
}



// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.


let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudent() {
        let count = 0
        this.students.forEach(student => {
            if (student.willPassWithHonors() === true) count++
        })
        return count
    },
    honorsPercentage() {
        let percent = (this.countHonorStudent() / this.students.length) * 100
        console.log(`${percent}%`)
    },
    retrieveHonorStudentInfo() {
        let honorStudents = []
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                    avgGrade: student.computeAve(),
                    email: student.email
                })
            }
        })
        return honorStudents
    },
    sortHonorStudentsByGradesDesc() {
        return this.retrieveHonorStudentInfo().sort((a, b) => b.avgGrade - a.avgGrade)
    }
}

    

