// What is the term given to unorganized code that's very hard to work with?
	- Spaghetti Code
	
// How are object literals written in JS?
	- {}

// What do you call the concept of organizing information and functionality to belong to an object?
	- Encapsulation 

// If the studentOne object has a method named enroll(), how would you invoke it?
	- studentOne.enroll()

// True or False: Objects can have objects as properties.
	- True

// What is the syntax in creating key-value pairs?
	- let object = {
		key: "value"
	}

// True or False: A method can have no parameters and still work.
	- True

// True or False: Arrays can have objects as elements.
	- True

// True or False: Arrays are objects.
	- True

// True or False: Objects can have arrays as properties.
	- True
